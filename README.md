# Codebase for the blog post

Steps to run this project:

1. Clone this Git repository: 'git clone https://gitlab.com/hendisantika/avoid-repeating-attributes-in-jpa-entities.git'
2. Navigate to the folder `avoid-repeating-attributes-in-jpa-entities`
3. Run this project with `mvn clean spring-boot:run`
4. Visit the H2 console at `http://localhost:8080/h2-console` (JDBC Url for the login form is: `jdbc:h2:mem:testdb`)

Images Screen shot

H2 Console

![H2 Console](img/h2.png "H2 Console")

Customer Record

![Customer Record](img/customer.png "Customer Record")

Product Record

![Product Record](img/product.png "Product Record")