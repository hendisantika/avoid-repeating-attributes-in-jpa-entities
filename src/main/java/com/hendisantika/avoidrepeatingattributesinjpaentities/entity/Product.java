package com.hendisantika.avoidrepeatingattributesinjpaentities.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Created by IntelliJ IDEA.
 * Project : avoid-repeating-attributes-in-jpa-entities
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/05/21
 * Time: 09.00
 */
@Data
@Entity
public class Product extends Audit {

    @Column(nullable = false, length = 100)
    private String name;

    private int amount;
}
