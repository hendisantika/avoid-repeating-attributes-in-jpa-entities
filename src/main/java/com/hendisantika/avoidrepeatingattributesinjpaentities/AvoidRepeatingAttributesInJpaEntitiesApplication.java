package com.hendisantika.avoidrepeatingattributesinjpaentities;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvoidRepeatingAttributesInJpaEntitiesApplication {

    public static void main(String[] args) {
        SpringApplication.run(AvoidRepeatingAttributesInJpaEntitiesApplication.class, args);
    }

}
