package com.hendisantika.avoidrepeatingattributesinjpaentities.repository;

import com.hendisantika.avoidrepeatingattributesinjpaentities.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : avoid-repeating-attributes-in-jpa-entities
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/05/21
 * Time: 09.03
 */
public interface ProductRepository extends JpaRepository<Product, Long> {
}